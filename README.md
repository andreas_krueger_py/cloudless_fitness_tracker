# Cloudless fitness

## What is this?

This is a simple fitness tracker.  All on your own hardware.  No cloud
gets your health data.

You go jogging (hiking, bike riding, ...) and monitor your own
activity as a GPS tracks.  This allows you to quickly pick the fastes
5 km (or any other distance) out of a (hopefully longer) track and
tells you what time you needed to cover that distances.

Just speed as a measure of fitness.  That's all there is to this.

This does not do heart rate, calories, or any such fancy stuff.  If
you find a way to cloudlessly obtain such data, feel free to grab
this and add.  This is open source, after all.

From a technical point of view, this is a Jupyter notebook (Python 3).
The notebook itself contains more detailed instructions; those are in
German, though.  The code itself is commented in English.

This was started when [VDES](https://www.vdes.org/) challenged all
employees of my current [employer](https://www.dbsystel.de/) (and
others of Deutsch Bahn) to leave the couch and go jogging, for the 5
km distance, during the early parts of the Corona crisis.

## Can I see a sample?

[Sure](Beispiel.pdf). It's in German, though.  Spoiler: Scroll down to
the very bottom first.

## How do I run this?

### Installation, done once

```
python3 -m venv python_venv
. python_venv/bin/activate
pip install -r requirements.txt
```

### Run

Put your own tracks anywhere below the `tracks` directory.  Files with
`.gpx` extension.  For the technically inclined: ... that contains XML
with `trkpt` elements in the `{http://www.topografix.com/GPX/1/1}`
namespace with at least `lat` and `lon` attributes and `time`
subelemts (the latter again in the same namespace).


```
. python_venv/bin/activate
jupyter notebook
```

This should open a browser window.  Choose the
`wolkenloser_tracker.ipynb` notebook, rerun, and have fun.

For rerunning, we recommend "Kernel / Restart and run all".

Or else reasonable shoes.  (This and other puns fully intended.)

